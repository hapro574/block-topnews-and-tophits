<!-- BEGIN: main -->
<div class="global-block-tab-groups">
    <ul class="nav nav-tabs">
        <!-- BEGIN: tab -->
        <li {TAB.active}>
            <a data-toggle="tab" href="#{TAB.href}"><strong>{TAB.tab_name}</strong></a>
        </li>
        <!-- END: tab -->
       
    </ul>
    <div class="tab-content">
        <!-- BEGIN: content -->
        <div id="{TAB.href}" {TAB.active}>
            <ul>
                <!-- BEGIN: row -->
                <li>
                    <a href="{ROW.link}" alt="{ROW.title}" target="{ROW.target}">{ROW.title}</a>
                    <p class="publtime">{ROW.publtime}</p>
                </li>
                <!-- END: row -->
                 <!-- BEGIN: newloop -->
                <li class="clearfix">
                    <!-- BEGIN: imgblock -->
                    <a title="{blocknews.title}" href="{blocknews.link}" {blocknews.target_blank}><img src="{blocknews.imgurl}" alt="{blocknews.title}" width="{blocknews.width}" class="img-thumbnail pull-left mr-1"/></a>
                    <!-- END: imgblock -->
                    <a {TITLE} class="show" href="{blocknews.link}" {blocknews.target_blank} data-content="{blocknews.hometext_clean}" data-img="{blocknews.imgurl}" data-rel="block_news_tooltip">{blocknews.title}</a>
                </li>
                <!-- END: newloop -->
            </ul>
        </div>
        <script>
            new PerfectScrollbar('#{TAB.href}');
        </script>
        <!-- END: content -->
    </div>
</div>


<!-- END: main -->