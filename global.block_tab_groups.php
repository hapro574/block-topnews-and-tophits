<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC <contact@vinades.vn>
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Sat, 10 Dec 2011 06:46:54 GMT
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

if (!nv_function_exists('nv_block_tab_groups')) {

    /**
     * nv_block_config_tab_groups()
     *
     * @param mixed $module
     * @param mixed $data_block
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_tab_groups($module, $data_block, $lang_block)
    {
        global $nv_Cache, $site_mods, $nv_Request;
        $tooltip_position = array(
            'top' => $lang_block['tooltip_position_top'],
            'bottom' => $lang_block['tooltip_position_bottom'],
            'left' => $lang_block['tooltip_position_left'],
            'right' => $lang_block['tooltip_position_right']
        );
        $html = '';
        if ($nv_Request->isset_request('loadajaxdata', 'get')) {
            $module = $nv_Request->get_title('loadajaxdata', 'get', '');
            $sql = 'SELECT * FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_block_cat ORDER BY weight ASC';
            $list = $nv_Cache->db($sql, '', $module);
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['tab1'] . ':</label>';
            $html .= '<div class="col-sm-9"><select name="config_tab1" class="form-control">';
            $html .= '<option value="0"> -- </option>';
            foreach ($list as $l) {
                $html .= '<option value="' . $l['bid'] . '" ' . (($data_block['tab1'] == $l['bid']) ? ' selected="selected"' : '') . '>' . $l['title'] . '</option>';
            }
            $html .= '</select>';

            $html .= '</div></div>';


            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['tab2'] . ':</label>';
            $sql = 'SELECT * FROM ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_cat ORDER BY sort ASC';
            $list = $nv_Cache->db($sql, '', $module);
            $html .= '<div class="col-sm-18">';
            $html .= '<div style="max-height: 200px; overflow: auto">';
            if (!is_array($data_block['tab2'])) {
                $data_block['tab2'] = explode(',', $data_block['tab2']);
            }
            foreach ($list as $l) {
                if ($l['status'] == 1 or $l['status'] == 2) {
                    $xtitle_i = '';

                    if ($l['lev'] > 0) {
                        for ($i = 1; $i <= $l['lev']; ++$i) {
                            $xtitle_i .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    $html .= $xtitle_i . '<label><input type="checkbox" name="config_nocatid[]" value="' . $l['catid'] . '" ' . ((in_array($l['catid'], $data_block['tab2'])) ? ' checked="checked"' : '') . '</input>' . $l['title'] . '</label><br />';
                }
            }
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';


            $html .= '<div class="form-group">';
            $html .= '	<label class="control-label col-sm-6">' . $lang_block['number_day'] . ':</label>';
            $html .= '	<div class="col-sm-18"><input type="text" name="config_number_day" class="form-control w100" size="5" value="' . $data_block['number_day'] . '"/></div>';
            $html .= '</div>';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['title_length'] . ':</label>';
            $html .= '<div class="col-sm-18"><input type="text" class="form-control" name="config_title_length" size="5" value="' . $data_block['title_length'] . '"/></div>';
            $html .= '</div>';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['numrow'] . ':</label>';
            $html .= '<div class="col-sm-18"><input type="text" class="form-control" name="config_numrow" size="5" value="' . $data_block['numrow'] . '"/></div>';
            $html .= '</div>';
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label col-sm-6">' . $lang_block['showtooltip'] . ':</label>';
            $html .= '<div class="col-sm-18">';
            $html .= '<div class="row">';
            $html .= '<div class="col-sm-4">';
            $html .= '<div class="checkbox"><label><input type="checkbox" value="1" name="config_showtooltip" ' . ($data_block['showtooltip'] == 1 ? 'checked="checked"' : '') . ' /></label>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="col-sm-10">';
            $html .= '<div class="input-group margin-bottom-sm">';
            $html .= '<div class="input-group-addon">' . $lang_block['tooltip_position'] . '</div>';
            $html .= '<select name="config_tooltip_position" class="form-control">';

            foreach ($tooltip_position as $key => $value) {
                $html .= '<option value="' . $key . '" ' . ($data_block['tooltip_position'] == $key ? 'selected="selected"' : '') . '>' . $value . '</option>';
            }

            $html .= '</select>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="col-sm-10">';
            $html .= '<div class="input-group">';
            $html .= '<div class="input-group-addon">' . $lang_block['tooltip_length'] . '</div>';
            $html .= '<input type="text" class="form-control" name="config_tooltip_length" value="' . $data_block['tooltip_length'] . '"/>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            nv_htmlOutput($html);
        }
        $html .= '<div class="form-group">';
        $html .= '<label class="control-label col-sm-6">' . $lang_block['selectmod'] . ':</label>';
        $html .= '<div class="col-sm-9">';
        $html .= '<select name="config_selectmod" class="form-control">';
        $html .= '<option value="">--</option>';
        $selectmod = '';
        $d = 0;
        foreach ($site_mods as $title => $mod) {
            if ($mod['module_file'] == 'news') {
                $d++;
                $html .= '<option value="' . $title . '" >' . $mod['custom_title'] . '</option>';
                $selectmod = $title;
            }
        }
        $data_block['selectmod'] = !empty($data_block['selectmod']) ? $data_block['selectmod'] : (($d == 1) ? $selectmod : '');
        $html .= '</select>';

        $html .= '
        <script type="text/javascript">
        $(\'[name="config_selectmod"]\').change(function() {
            var mod = $(this).val();
            var file_name = $("select[name=file_name]").val();
            var module_type = $("select[name=module_type]").val();
            var blok_file_name = "";
            if (file_name != "") {
                var arr_file = file_name.split("|");
                if (parseInt(arr_file[1]) == 1) {
                    blok_file_name = arr_file[0];
                }
            }
            if (mod != "") {
                $.get(script_name + "?" + nv_name_variable + "=" + nv_module_name + \'&\' + nv_lang_variable + "=" + nv_lang_data + "&" + nv_fc_variable + "=block_config&bid=" + bid + "&module=" + module_type + "&selectthemes=" + selectthemes + "&file_name=" + blok_file_name + "&loadajaxdata=" + mod + "&nocache=" + new Date().getTime(), function(theResponse) {
                    $("#block_config_extend").empty().append(theResponse);
                });
            }
        });
        $(function() {
            $(\'[name="config_selectmod"]\').val(\'' . $data_block['selectmod'] . '\');
            $(\'[name="config_selectmod"]\').change();
        });
        </script>
        ';

        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div id="block_config_extend">';
        $html .= '</div>';

        return $html;
    }

    /**
     * nv_block_config_tab_groups_submit()
     *
     * @param mixed $module
     * @param mixed $lang_block
     * @return
     */
    function nv_block_config_tab_groups_submit($module, $lang_block)
    {
        global $nv_Request;
        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['selectmod'] = $nv_Request->get_title('config_selectmod', 'post', '');
        $return['config']['tab1'] = $nv_Request->get_int('config_tab1', 'post', 0);
        $return['config']['tab2'] = $nv_Request->get_typed_array('config_tab2', 'post', 'int', array());
        $return['config']['numrow'] = $nv_Request->get_int('config_numrow', 'post', 0);
        $return['config']['title_length'] = $nv_Request->get_int('config_title_length', 'post', 20);
        $return['config']['showtooltip'] = $nv_Request->get_int('config_showtooltip', 'post', 0);
        $return['config']['tooltip_position'] = $nv_Request->get_string('config_tooltip_position', 'post', 0);
        $return['config']['tooltip_length'] = $nv_Request->get_string('config_tooltip_length', 'post', 0);
        $return['config']['number_day'] = $nv_Request->get_int('config_number_day', 'post', 0);
        return $return;
    }

    /**
     * nv_block_tab_groups()
     *
     * @param mixed $block_config
     * @return
     */
    function nv_block_tab_groups($block_config)
    {
        global $site_mods, $module_config, $global_config, $nv_Cache, $db,$data_block,$module_array_cat,$db_slave ;

        $block_config['module'] = $block_config['selectmod'];
        $module = $block_config['module'];
        $mod_file = $site_mods[$module]['module_file'];

        $blockwidth = $module_config[$module]['blockwidth'];
        $show_no_image = $module_config[$module]['show_no_image'];
        $publtime = NV_CURRENTTIME - $block_config['number_day'] * 86400;

        $array_block_news = array();


        $block_data = array();


        foreach (array(
            $block_config['tab1']
        ) as $bid) {
            if (empty($bid))
                continue;
            $db->sqlreset()
                ->select('t1.id, t1.catid, t1.title, t1.alias, t1.homeimgfile, t1.homeimgthumb,t1.hometext,t1.publtime,t1.external_link,t3.title as tab_name,t3.alias as href')
                ->from(NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_rows t1')
                ->join('INNER JOIN ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_block t2 ON t1.id = t2.id' . ' INNER JOIN ' . NV_PREFIXLANG . '_' . $site_mods[$module]['module_data'] . '_block_cat t3 ON t2.bid = t3.bid')
                ->where('t2.bid= ' . $bid . ' AND t1.status= 1')
                ->order('t2.weight ASC')
                ->limit($block_config['numrow']);
            $list = $nv_Cache->db($db->sql(), '', $module);
            if (empty($list))
                continue;
            $block_data[] = array(
                'tab_name' => $list[0]['tab_name'],
                'href' => $list[0]['href'],
                'content' => $list
            );
        }




        $db_slave->sqlreset()
        ->select('id, catid, publtime, title, alias, homeimgthumb, homeimgfile, hometext, external_link')
        ->from(NV_PREFIXLANG . '_' . $mod_data . '_rows')
        ->order('hitstotal DESC')
        ->limit($block_config['numrow']);
        if (empty($block_config['nocatid'])) {
            $db_slave->where('status= 1 AND publtime > ' . $publtime);
        } else {
            $db_slave->where('status= 1 AND publtime > ' . $publtime . ' AND catid NOT IN (' . implode(',', $block_config['tab2']) . ')');
        }

        $result = $db_slave->query($db_slave->sql());
        while (list ($id, $catid, $publtime, $title, $alias, $homeimgthumb, $homeimgfile, $hometext, $external_link) = $result->fetch(3)) {
            if ($homeimgthumb == 1) {
                // image thumb
                $imgurl = NV_BASE_SITEURL . NV_FILES_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $homeimgfile;
            } elseif ($homeimgthumb == 2) {
                // image file
                $imgurl = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $site_mods[$module]['module_upload'] . '/' . $homeimgfile;
            } elseif ($homeimgthumb == 3) {
                // image url
                $imgurl = $homeimgfile;
            } elseif (!empty($show_no_image)) {
                // no image
                $imgurl = NV_BASE_SITEURL . $show_no_image;
            } else {
                $imgurl = '';
            }
            $link = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $module_array_cat[$catid]['alias'] . '/' . $alias . '-' . $id . $global_config['rewrite_exturl'];

            $array_block_news[] = array(
                'id' => $id,
                'title' => $title,
                'link' => $link,
                'imgurl' => $imgurl,
                'width' => $blockwidth,
                'hometext' => $hometext,
                'external_link' => $external_link
            );
        }


        if (file_exists(NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/' . $block_config['block_name'] . '.tpl')) {
            $block_theme = $global_config['module_theme'];
        } elseif (file_exists(NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/' . $block_config['block_name'] . '.tpl')) {
            $block_theme = $global_config['site_theme'];
        } else {
            $block_theme = 'default';
        }
        $xtpl = new XTemplate($block_config['block_name'] . '.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks');
        $xtpl->assign('NV_BASE_SITEURL', NV_BASE_SITEURL);
        $xtpl->assign('TEMPLATE', $block_theme);
        $active = true;
        foreach ($block_data as $tab) {
            $tab['active'] = $active ? "class=\"active\"" : "";
            $active = false;
            $xtpl->assign('TAB', $tab);
            $xtpl->parse('main.tab');
        }
        $active = true;
        foreach ($block_data as $tab) {
            $tab['active'] = $active ? "class=\"tab-pane fade active in\"" : "class=\"tab-pane fade\"";
            $active = false;
            $xtpl->assign('TAB', $tab);
            foreach ($tab['content'] as $content) {
                $content['link'] = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $module_array_cat[$content['catid']]['alias'] . '/' . $content['alias'] . '-' . $content['id'] . $global_config['rewrite_exturl'];
                $content['publtime'] = nv_date('d/m/Y h:m:s', $content['publtime']);
                $content['target'] = $content['external_link'] ? '_blank' : '_self';
                $xtpl->assign('ROW', $content);
                $xtpl->parse('main.content.row');
            }
            $xtpl->parse('main.content');
        }

        foreach ($array_block_news as $array_news) {
            $array_news['hometext_clean'] = strip_tags($array_news['hometext']);
            $array_news['hometext_clean'] = nv_clean60($array_news['hometext_clean'], $block_config['tooltip_length'], true);

            if ($array_news['external_link']) {
                $array_news['target_blank'] = 'target="_blank"';
            }

            $xtpl->assign('blocknews', $array_news);

            if (!empty($array_news['imgurl'])) {
                $xtpl->parse('main.newloop.imgblock');
            }

            if (!$block_config['showtooltip']) {
                $xtpl->assign('TITLE', 'title="' . $array_news['title'] . '"');
            }

            $xtpl->parse('main.newloop');
        }

        $xtpl->parse('main');
        return $xtpl->text('main');
    }
}

if (defined('NV_SYSTEM')) {
    global $nv_Cache, $site_mods, $module_name, $global_array_cat, $module_array_cat;
    $module = $block_config['module'];
    if (isset($site_mods[$module])) {
        $mod_data = $site_mods[$module]['module_data'];
        if ($module == $module_name) {
            $module_array_cat = $global_array_cat;
            unset($module_array_cat[0]);
        } else {
            $module_array_cat = array();
            $sql = 'SELECT catid, parentid, title, alias, viewcat, subcatid, numlinks, description, keywords, groups_view, status FROM ' . NV_PREFIXLANG . '_' . $mod_data . '_cat ORDER BY sort ASC';
            $list = $nv_Cache->db($sql, 'catid', $module);
            if(!empty($list))
            {
                foreach ($list as $l) {
                    $module_array_cat[$l['catid']] = $l;
                    $module_array_cat[$l['catid']]['link'] = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=' . $l['alias'];
                }
            }
        }
        $content = nv_block_tab_groups($block_config, $mod_data);
    }
}